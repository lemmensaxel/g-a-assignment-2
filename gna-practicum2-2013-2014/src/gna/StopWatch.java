package gna;
public class StopWatch {
    
    private long startTime = 0;
    private long stopTime = 0;
    private long startTime_ms = 0;
    private long stopTime_ms = 0;
    private boolean running = false;

    
    public void start() {
        this.startTime = System.nanoTime();			
        this.startTime_ms = System.currentTimeMillis();
        this.running = true;
    }

    
    public void stop() {
        this.stopTime = System.nanoTime();
        this.stopTime_ms = System.currentTimeMillis();
        this.running = false;
    }

    
    //elaspsed time in nanosecods
    public long getElapsedTimeinNanoSeconds() {
        long elapsed;
        if (running) {
             elapsed = (System.nanoTime() - startTime);
        }
        else {
            elapsed = (stopTime - startTime);
        }
        return elapsed;
    }
    
    //elaspsed time in milliseconds
    public long getElapsedTimeinMillis() {
        long elapsed;
        if (running) {
             elapsed = (System.currentTimeMillis() - startTime_ms);
        }
        else {
            elapsed = (stopTime_ms - startTime_ms);
        }
        return elapsed;
    }
    
    
    //elaspsed time in seconds
    public long getElapsedTimeSecs() {
        long elapsed;
        if (running) {
            elapsed = ((System.currentTimeMillis() - startTime_ms) / 1000);
        }
        else {
            elapsed = ((stopTime_ms - startTime_ms) / 1000);
        }
        return elapsed;
    }
    
    //reset the elapsed time
    public void reset() {
    	this.startTime = 0;
    	stopTime = 0;
    }
    
}