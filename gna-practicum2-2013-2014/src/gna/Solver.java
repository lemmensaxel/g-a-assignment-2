package gna;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Stack;

import libpract.PriorityFunc;

public class Solver
{
	Stack<Board> stack = new Stack<Board>();
	PriorityQueue<Board> queue = new PriorityQueue<Board>();
	Boolean done = false;
	Board current;
	Board previous;
	Board end;
	//Board initial;
	int moves = 0;
	PriorityFunc priority;
	int counter;
	Board initial;
	ArrayList<Board> sol = new ArrayList<Board>();
	/**
	 * Finds a solution to the initial board.
	 *
	 * @param priority is either PriorityFunc.HAMMING or PriorityFunc.MANHATTAN
	 */
	public Solver(Board initial, PriorityFunc priority)
	{
		// Use the given priority function (either PriorityFunc.HAMMING
		// or PriorityFunc.MANHATTAN) to solve the puzzle.
		this.initial = initial;
		this.priority = priority;
		System.out.println(initial);
		if (priority == PriorityFunc.HAMMING) {
			queue.add(initial);
			previous = initial;
			done = false;
			while (!done) {
				current = queue.poll();
				if (current.getPrevious() != null) {
					previous = current.getPrevious();
				}
				if (current.equals(initial.solution())) {
					done = true;
					end = current;
				} else {
					for (Board board : current.neighbors()) {
						if(!previous.equals(board))
							queue.add(board);
					}
				}
			}
			
			
		} else if (priority == PriorityFunc.MANHATTAN) {
			initial.setHamming(false);
			queue.add(initial);
			previous = initial;
			done = false;
			while (!done) {
				current = queue.poll();
				if (current.getPrevious() != null) {
					previous = current.getPrevious();
				}
				if (current.equals(initial.solution())) {
					done = true;
					end = current;
				} else {
					for (Board board : current.neighbors()) {
						if (!previous.equals(board)) {
							board.setHamming(false);
							queue.add(board);
						}
					}
				}
			}
			
		} else {
			throw new IllegalArgumentException("Priority function not supported");
		}
		
	}
	
	/**
	 * Returns min number of moves to solve initial board. -1 if no solution.
	 */
	public int moves()
	{
		
		if (sol.isEmpty()) {
			solution();
			return sol.size();
		} else {
			return sol.size();
		}
	}
	
	/**
	 * Returns an Iterable of board positions in solution.
	 */
	public Iterable<Board> solution()
	{
		Board prev = end;
		while (prev.getPrevious() != null) {
			stack.add(prev);
			prev = prev.getPrevious();
		}
		while(!stack.isEmpty()) {
			sol.add(stack.pop());
		}
		
		return sol;
	}
}