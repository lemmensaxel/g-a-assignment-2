package gna;

import java.util.ArrayList;
import java.util.Arrays;

public class Board implements Comparable<Board> {
	// INITIALISING VARIABLES
	private final int dimension;
	private final int[][] tiles;
	private boolean hamming = true;
	private Board previous = null;
	private int moves = 0;
	private int[][] tiles_copy;

	// construct a board from an N-by-N array of tiles
	public Board(int[][] tiles) {
		this.dimension = tiles.length;
		this.tiles = tiles;
		tiles_copy = copy_matrix(tiles);
	}
	
	//Method to return the array of the current board.
	public int[][] getTiles() {
		return this.tiles;
	}

	// return number of blocks out of place
	public int hamming() {
		int hammingDistance = 0;
		for (int i = 0; i < dimension; ++i) {
			for (int j = 0; j < dimension; ++j) {
				if ((tiles[i][j] != 0) && !(tiles[i][j] == (dimension * i) + (j + 1)))
					hammingDistance++;
			}
		}
		return hammingDistance + calculateMoves();
	}

	// return sum of Manhattan distances between blocks and goal
	public int manhattan() {
		int totalSum = 0;
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				if (tiles[i][j] != 0) {
					int targetX = (tiles[i][j] - 1) / dimension;
					int targetY = (tiles[i][j] - 1) % dimension;
					int x = i - targetX;
					int y = j - targetY;
					totalSum += Math.abs(x) + Math.abs(y);
				}

			}
		}
		
		return totalSum+calculateMoves();
	}

	// does this board position equal y
	public boolean equals(Object y) {
		if (y == null) return false;
		if (y.getClass() == this.getClass()) {
			if (this == y) {
				return true;
			}
			int [][] compare_array = ((Board) y).getTiles();
			if (Arrays.deepEquals(this.getTiles(), compare_array)) {
				return true;
			} else {
				return false;
			}
		} else {
			throw new RuntimeException("The provided object was not of the class Board.");
		}
	}

	// return an Iterable of all neighboring board positions
	public Iterable<Board> neighbors() {
		
		int x = 0;
		int y = 0;

		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				if (tiles[i][j] == 0) {
					x = i;
					y = j;
					// stop loop
					i = dimension;
					j = dimension;
				}
			}
		}
		
		int [][] right = copy_matrix(tiles);
		int [][] left = copy_matrix(tiles);
		int [][] top = copy_matrix(tiles);
		int [][] bottom = copy_matrix(tiles);
		
		if (y+1 < right[0].length) {
			swap(right, x, y, x, y+1);
		} else {
			right = null;
		}
		
		if (y-1 >= 0) { 
			swap(left, x, y, x, y-1);
		} else {
			left = null;
		}
		
		if (x+1 < bottom.length) {
			swap(bottom, x, y, x+1, y);
		} else {
			bottom = null;
		}
		
		if (x-1 >= 0) {
			swap(top, x, y, x-1, y);
		} else {
			top = null;
		}
		
		ArrayList<Board> neighbors = new ArrayList<Board>();
		if (right != null) {
			Board right_board = new Board(right);
			right_board.setPrevious(this);
			if (this.hamming) {
				right_board.setHamming(true);
			} else {
				right_board.setHamming(false);
			}
			neighbors.add(right_board);
		}
		if (left != null) {
			Board left_board = new Board(left);
			left_board.setPrevious(this);
			if (this.hamming) {
				left_board.setHamming(true);
			} else {
				left_board.setHamming(false);
			}
			neighbors.add(left_board);
		}
		if (top != null) {
			Board top_board = new Board(top);
			top_board.setPrevious(this);
			if (this.hamming) {
				top_board.setHamming(true);
			} else {
				top_board.setHamming(false);
			}
			neighbors.add(top_board);
		}
		if (bottom != null) {
			Board bottom_board = new Board(bottom);
			bottom_board.setPrevious(this);
			if (this.hamming) {
				bottom_board.setHamming(true);
			} else {
				bottom_board.setHamming(false);
			}
			neighbors.add(bottom_board);
		}
		return neighbors;
		
	}

	// return a string representation of the board
	public String toString() {
		String concat = "";
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
					concat = concat + " " + tiles[i][j];
				}
			concat= concat + "\n";
			}
		return concat;
	}

	// is the initial board solvable?
	public boolean isSolvable() {
		double result = 1;
		setZero();
		// Linearising the array
		ArrayList<Integer> array = new ArrayList<Integer>();
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
					array.add(tiles_copy[i][j]);
			}
		}

		// Calculating
		double numerator = 0;
		double denominator = 0;
		
		for (int j = 2; j < array.size(); j++) {
			for (int i = 1; i < j; i++) {
				 result *= (p(j, array) - p(i, array));
				 result = result /  (double)(j-i);
			}
		}
		
		return result > 0;
	}

	
	/**
	 * A method that swaps 2 elements in a provided array
	 * @param array
	 * @param a
	 * @param b
	 * @param c
	 * @param d
	 */
	public void swap(int[][] array, int a, int b, int c, int d) {
		int temp = array[a][b];
		array[a][b] = array[c][d];
		array[c][d] = temp;
	}
	
	
	/**
	 * A method that slides zero to the right-hand bottom.
	 */
	public void setZero() {
		int x = 0;
		int y = 0;

		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				if (tiles_copy[i][j] == 0) {
					x = i;
					y = j;
					// stop loop
					i = dimension;
					j = dimension;
				}
			}
		}
		for (int z = y; z+1 < dimension; z++) {
			swap(tiles_copy, x, z, x, z+1);
			y++;
		}
		
		for (int z = x ; z+1 < dimension; z++) {
			swap(tiles_copy, z, y, z+1, y);
		}

	}

	/**
	 * A method that returns the index of a given value in the provided ArrayList
	 * @param value
	 * @param array
	 * @return
	 */
	public double p(int value, ArrayList<Integer> array) {
		for (int i = 0; i < array.size(); i++) {
			if (array.get(i) == value) {
				return (double) i + 1;
			}
		}
		return (double) 0;
	}
	/**
	 * A method that deep-copys an array to another array.
	 * @param input
	 */
	public static int[][] copy_matrix(int[][] input) {
	    if (input == null)
	        return null;
	    int[][] result = new int[input.length][];
	    for (int r = 0; r < input.length; r++) {
	        result[r] = input[r].clone();
	    }
	    return result;
	}
	
	/**
	 * A method that returns the solved board of the current board.
	 * @return
	 */
	public Board solution() {
		int counter = 1;
		int [][] solution = new int[dimension][dimension];
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				if ((i == dimension-1) && (j == dimension-1)) {
					solution[i][j] = 0;
				} else {
					solution[i][j] = counter;
					counter++;
				}
			}
		}
		Board board = new Board(solution);
		return board;
	}
	
	/**
	 * A method to set if the priority-algorithm should be Hamming or Manhattan.
	 * @param hamming
	 */
	public void setHamming(boolean hamming) {
		this.hamming = hamming;
	}
	
	/**
	 * The compare-method for our PriorityQueue
	 */
	@Override
	public int compareTo(Board board)  {
		if (hamming) {
			if (this.hamming() < board.hamming()) {
				return -1;
			} else if (this.hamming() > board.hamming()) {
				return 1;
			} else {
				return 0;
			}
		} else {
			if (this.manhattan() < board.manhattan()) {
				return -1;
			} else if (this.manhattan() > board.manhattan()) {
				return 1;
			} else {
				return 0;
			}
		}
	}
	
	/**
	 * A method to set the previous board.
	 * @param previous
	 */
	public void setPrevious(Board previous) {
		this.previous = previous;
	}
	
	/**
	 * A method to get the previous board.
	 * @return
	 */
	public Board getPrevious() {
		return previous;
	}
	
	/**
	 * A method that calculates the amount of moves to this board.
	 */
	public int calculateMoves() {
		moves = 0;
		Board temp = previous;
		while(temp != null) {
			temp = temp.getPrevious();
			moves++;
		}
		return moves;
	}
	
	/**
	 * A method that returns the amount of moves.
	 */
	public int getMoves() {
		return moves;
	}
	
}
