package gna;

import java.util.ArrayList;

import libpract.PriorityFunc;
import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

/**
 * A number of JUnit tests for Solver.
 * 
 * Feel free to modify these to automatically test puzzles or other
 * functionality
 */
public class UnitTests {

	@Test
	public void manhattenTest() {
		int[][] array = new int[][] { { 8, 4, 7 }, { 1, 5, 6 }, { 3, 2, 0 }, };

		Board bord = new Board(array);
		int manhattenSum = bord.manhattan();

		assertEquals(16, manhattenSum);
	}

	@Test
	public void hammingTest() {
		int[][] array = new int[][] { { 8, 4, 7 }, { 1, 5, 6 }, { 3, 2, 0 }, };

		Board bord = new Board(array);
		int hammingSum = bord.hamming();

		assertEquals(6, hammingSum);
	}

	@Test
	public void isSolvableTest_illegalCase() {
		int[][] array = new int[][] { { 1, 2, 3 }, { 4, 5, 6 }, { 8, 7, 0 } };
		Board bord = new Board(array);

		assertEquals(false, bord.isSolvable());
	}
	
	@Test
	public void isSolvableTest_illegalCase2() {
		int[][] array = new int[][] { { 1, 3, 2 }, { 4, 6, 5 }, { 8, 7, 0 } };
		Board bord = new Board(array);

		assertEquals(false, bord.isSolvable());
	}

	@Test
	public void isSolvableTest_legalCase() {
		int[][] array = new int[][] { { 0, 1, 3 }, { 4, 2, 5 }, { 7, 8, 6 } };
		Board bord = new Board(array);

		assertEquals(true, bord.isSolvable());
	}
	
	@Test
	public void isSolvableTest_legalCase2() {
		int[][] array = new int[][] { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 } };
		Board bord = new Board(array);

		assertEquals(true, bord.isSolvable());
	}
	
	@Test
	public void isSolvableTest_legalCase3() {
		int[][] array = new int[][] { { 6, 5, 11, 4 }, { 10, 13, 2, 1 }, { 9, 15, 7, 3 }, {14, 12, 8, 0} };
		Board bord = new Board(array);

		assertEquals(true, bord.isSolvable());
	}
	
	@Test
	public void equals_legalcase() {
		int[][] array = new int[][] { { 0, 1, 3 }, { 4, 2, 5 }, { 7, 8, 6 } };
		Board bord = new Board(array);
		Board bord2 = new Board(array);
		assertEquals(true, bord.equals(bord2));
	}

	@Test
	public void equals_illegalcase() {
		int[][] array = new int[][] { { 0, 1, 3 }, { 4, 2, 5 }, { 7, 8, 6 } };
		int[][] array2 = new int[][] { { 1, 3, 2 }, { 4, 6, 5 }, { 8, 7, 0 } };
		Board bord = new Board(array);
		Board bord2 = new Board(array2);
		assertEquals(false, bord.equals(bord2));
	}
	
	@Test
	public void isSolvableTest_1() {
		int[][] array = new int[][] { { 8, 1, 3 }, { 4, 0, 2 }, { 7, 6, 5 } };
		Board bord = new Board(array);

		assertEquals(true, bord.isSolvable());
	}
}
